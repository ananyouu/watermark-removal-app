<p align="center">
	<img alt="logo" src="https://rlxx.vip/t/2025/02/14/67af08ea2f260.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">依沫水印去除</h1>
<h4 align="center">基于UniApp开发</h4>
<p align="center">
	<img src="https://img.shields.io/github/license/mashape/apistatus.svg">
</p>

## 平台简介

支持抖音，快手，小红书，哔哩哔哩等 100+ 平台的图片视频水印去除，源码下载即用，这些内容展示均已实现微信小程序流量主广告变现功能，可根据自己是否需要进行配置。
* 应用框架基于[uniapp](https://uniapp.dcloud.net.cn/)，支持小程序、H5、Android和IOS。（流量主流量变现广告目前只支持微信小程序）
* 前端组件采用[uni-ui](https://github.com/dcloudio/uni-ui)，全端兼容的高性能UI框架。
* 阿里云服务器免费试用3个月，新老用户同享 99/年：[点我进入](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=ua4mkdxk)
* 此版本为过度纯前端版本，没有后台管理，最近几天将上线带管理后台版本的去水印，后台管理系统现实现功能有：每日数据的统计和图标展示、用户管理（数据库）、每日用量管理（redis）、解析记录、新增download域名、系统参数设置、通告设置，功能如下图所示。
<img src="https://rlxx.vip/t/2025/02/14/67af09f02f207.png"  alt="登录"/>
<img src="https://rlxx.vip/t/2025/02/14/67af09f0bbe0e.png"  alt="首页"/>
<img src="https://rlxx.vip/t/2025/02/14/67af09f0f3d50.png" alt="用户管理"/>
* 如需要带后台管理系统的版本请打开微信搜索公众号关注 “依沫资源网” 或微信扫描下方二维码关注微信公众号并发送 "去水印后台管理" 即可订阅该版本，订阅后发布该版本将第一时间邀请你体验（该说明如长时间未更新可能该后台版本已发布，请在公众号内直接发送 "去水印后台管理" 即可获取下载链接）

<img src="https://rlxx.vip/t/2025/02/14/67af0998ebc43.png" alt="公众号"/>

## 技术文档

[//]: # (<img src="http://img.eplusskin.com/uploads/20240730/1715928722339.png">  QQ： 1149297946 或 微信：yimoziyuan666)
- 运行前请点开 manifest.json 获取自己的 uniapp应用标识（AppID）
- 如需微信小程序运行或实现微信流量主小程序流量变现请修改 manifest.json 微信小程序的AppID，以及在代码里配置自己的广告id
- 该小程序还未发布，您可以打开微信扫描下面的小程序码即可体验已发布且嫁接过此去水印功能的小程序，（请前往小程序里的去水印功能，该演示小程序也已开源，如有需要） [点击这里前往](https://ext.dcloud.net.cn/plugin?id=18374)

<img src="https://rlxx.vip/t/2025/02/14/67af099920fb8.jpg" alt="小程序演示"/>

## 技术支持

- 如有问题欢迎联系作者

<table>
    <tr>
        <td align="center" bgcolor=#F6F8FA>微信</td>
        <td align="center" bgcolor=#F6F8FA>QQ</td>
    </tr>
    <tr>
        <td bgcolor=#FFFFFF><img src="https://rlxx.vip/t/2025/02/14/67af09f3d75af.png"/></td>
        <td bgcolor=#FFFFFF><img src="https://rlxx.vip/t/2025/02/14/67af09993b307.png"/></td>
    </tr>
    <tr>
        <td align="center">YiMoZiYuan666</td>
        <td align="center">1 1 4 9 2 9 7 9 4 6</td>
    </tr>
</table>
## 小程序部分演示图

<table>
    <tr>
        <td><img src="https://rlxx.vip/t/2025/02/14/67af09c8d2099.png"/></td>
        <td><img src="https://rlxx.vip/t/2025/02/14/67af09f464dc8.png"/></td>
        <td><img src="https://rlxx.vip/t/2025/02/14/67af09c8ed37b.png"/></td>
    </tr>
</table>

* 若您喜欢对您有帮助的话，麻烦客官动动发财的手给小二一个 Star ，您的支持是我们不断进步的源泉！！！

